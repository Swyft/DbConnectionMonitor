﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.Tests.Specifications.Data;

namespace Utilities.Tests.Specifications
{
    [TestClass]
    public class AndNotSpecificationTest
    {
        #region Public Methods and Operators

        [TestMethod]
        public void Should_Be_Satisfied_When_LeftSpecification_Is_Satisfied_And_Right_Specification_IsNot_Satisfied()
        {
            var c = CustomerGenerator.GetUsAdultCustomer();
            new AdultCustomerSpecification().AndNot(new FrenchCustomerSpecification()).IsSatisfiedBy(c).Should().BeTrue();
        }

        [TestMethod]
        public void Should_Not_Be_Satisfied_When_LeftSpecification_Is_Satisfied_And_RightSpecification_Is_Satisfied()
        {
            var c = CustomerGenerator.GetFrenchAdultCustomer();
            new AdultCustomerSpecification().AndNot(new FrenchCustomerSpecification()).IsSatisfiedBy(c).Should().BeFalse();
        }

        [TestMethod]
        public void Should_Not_Be_Satisfied_When_LeftSpecification_IsNot_Satisfied_And_RightSpecification_Is_Satisfied()
        {
            var c = CustomerGenerator.GetFrenchMinorCustomer();
            new AdultCustomerSpecification().AndNot(new FrenchCustomerSpecification()).IsSatisfiedBy(c).Should().BeFalse();
        }

        [TestMethod]
        public void Should_Not_Be_Satisfied_When_LeftSpecification_IsNot_Satisfied_And_RightSpecification_IsNot_Satisfied()
        {
            var c = CustomerGenerator.GetUsMinorCustomer();
            new AdultCustomerSpecification().AndNot(new FrenchCustomerSpecification()).IsSatisfiedBy(c).Should().BeFalse();
        }

        #endregion
    }
}