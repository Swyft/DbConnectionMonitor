﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.Tests.Specifications.Data;

namespace Utilities.Tests.Specifications
{
    [TestClass]
    public class NotSpecificationTest
    {
        #region Public Methods and Operators

        [TestMethod]
        public void Should_Be_Satisfied_When_Specification_Is_Not_Satisfied()
        {
            var c = CustomerGenerator.GetFrenchMinorCustomer();
            new AdultCustomerSpecification().Not().IsSatisfiedBy(c).Should().BeTrue();
        }

        [TestMethod]
        public void Should_Not_Be_Satisfied_When_Specification_Is_Satisfied()
        {
            var c = CustomerGenerator.GetFrenchAdultCustomer();
            new AdultCustomerSpecification().Not().IsSatisfiedBy(c).Should().BeFalse();
        }

        #endregion
    }
}