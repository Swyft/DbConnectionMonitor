namespace Utilities.Tests.Specifications.Data
{
    internal class Customer
    {
        #region Public Properties

        public int GetAge { get; private set; }

        public string GetCountryIsoAlpha2Code { get; private set; }

        #endregion

        #region Methods

        internal Customer Age(int age)
        {
            this.GetAge = age;
            return this;
        }

        internal Customer CountryIsoAlpha2Code(string countryIsoAlpha2Code)
        {
            this.GetCountryIsoAlpha2Code = countryIsoAlpha2Code;
            return this;
        }

        #endregion
    }
}