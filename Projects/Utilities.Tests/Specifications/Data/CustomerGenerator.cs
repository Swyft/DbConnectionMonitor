﻿namespace Utilities.Tests.Specifications.Data
{
    internal static class CustomerGenerator
    {
        #region Methods

        internal static Customer GetFrenchAdultCustomer()
        {
            return new Customer().Age(18).CountryIsoAlpha2Code("FR");
        }

        internal static Customer GetFrenchMinorCustomer()
        {
            return new Customer().Age(15).CountryIsoAlpha2Code("FR");
        }

        internal static Customer GetUsAdultCustomer()
        {
            return new Customer().Age(18).CountryIsoAlpha2Code("US");
        }

        internal static Customer GetUsMinorCustomer()
        {
            return new Customer().Age(15).CountryIsoAlpha2Code("US");
        }

        #endregion
    }
}