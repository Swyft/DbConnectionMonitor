﻿namespace Utilities.Tests.Specifications.Data
{
    using Utilities.Specifications;

    internal class FrenchCustomerSpecification : CompositeSpecification<Customer>
    {
        #region Constants

        private const string FrenchCountryAlpha2Code = "FR";

        #endregion

        #region Public Methods and Operators

        public override bool IsSatisfiedBy(Customer candidate)
        {
            return candidate.GetCountryIsoAlpha2Code == FrenchCountryAlpha2Code;
        }

        #endregion
    }
}