﻿namespace Utilities.Tests.Specifications.Data
{
    using Utilities.Specifications;

    internal class AdultCustomerSpecification : CompositeSpecification<Customer>
    {
        #region Constants

        private const int FrenchAgeOfMajority = 18;

        #endregion

        #region Public Methods and Operators

        public override bool IsSatisfiedBy(Customer candidate)
        {
            return candidate.GetAge >= FrenchAgeOfMajority;
        }

        #endregion
    }
}