﻿using System;
using System.Collections.Generic;
using System.Linq;
using DbConnectionMonitor.Specifications;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace DbConnectionMonitor.Tests
{
    [TestClass]
    public class ConnectionMonitorTest
    {
        #region Public Methods and Operators

        [TestMethod]
        public void Sould_Not_Return_LeakConnection_When_Closing_LeakConnections()
        {
            var connection1 = Substitute.For<IConnectionInfo>();
            var open = true;
            connection1.IsOpen.Returns(true);
            connection1.TimeOpen.Returns(new TimeSpan(0, 0, 200));
            connection1.When(x => x.Close()).Do(info => open = false);

            var connection2 = Substitute.For<IConnectionInfo>();
            connection2.IsOpen.Returns(true);
            connection2.TimeOpen.Returns(new TimeSpan(0, 0, 160));

            var repo = Substitute.For<IConnectionInfoRepository>();
            repo.GetAll().Returns(new List<IConnectionInfo> {connection1, connection2});
            var cm = new ConnectionMonitor(repo);
            cm.CloseLeakConnectionsInfos();

            connection1.IsOpen.Returns(open);
            cm.GetLeakConnectionsInfos().Should().BeEmpty();
        }

        [TestMethod]
        public void Sould_Return_Empty_Connection_When_Clearing_Monitor()
        {
            var connection1 = Substitute.For<IConnectionInfo>();
            var connection2 = Substitute.For<IConnectionInfo>();
            var connections = new List<IConnectionInfo> {connection1, connection2};

            var repo = Substitute.For<IConnectionInfoRepository>();
            repo.GetAll().Returns(connections);

            repo.When(x => x.RemoveAll()).Do(x => connections.Clear());

            var cm = new ConnectionMonitor(repo);
            cm.Clear();

            repo.Received().RemoveAll();
            cm.GetConnectionsInfos().Should().BeEmpty();
        }

        [TestMethod]
        public void Sould_Return_LeakConnection_When_Contains_Connection_Open_From_At_Least_180_Second()
        {
            var connection1 = Substitute.For<IConnectionInfo>();
            connection1.IsOpen.Returns(true);
            connection1.TimeOpen.Returns(new TimeSpan(0, 0, 200));

            var connection2 = Substitute.For<IConnectionInfo>();
            connection2.IsOpen.Returns(true);
            connection2.TimeOpen.Returns(new TimeSpan(0, 0, 160));

            var repo = Substitute.For<IConnectionInfoRepository>();
            repo.GetAll().Returns(new List<IConnectionInfo> {connection1, connection2});
            var cm = new ConnectionMonitor(repo);
            cm.GetLeakConnectionsInfos().Should().HaveCount(1);
        }

        [TestMethod]
        public void Sould_Return_One_Connection_When_Adding_One_Connection()
        {
            var connection1 = Substitute.For<IConnectionInfo>();
            var repo = Substitute.For<IConnectionInfoRepository>();
            repo.GetAll().Returns(new List<IConnectionInfo> {connection1});
            var cm = new ConnectionMonitor(repo);
            cm.Add(connection1);

            repo.Received().Add(connection1);
            cm.GetConnectionsInfos().Should().HaveCount(1);
        }

        [TestMethod]
        public void Sould_Return_Only_Open_Connection_When_Removing_Closed_Connection()
        {
            var connection1 = Substitute.For<IConnectionInfo>();
            connection1.IsOpen.Returns(true);

            var connection2 = Substitute.For<IConnectionInfo>();
            connection1.IsOpen.Returns(false);

            var connections = new List<IConnectionInfo> {connection1, connection2};
            var connectionsExpected = new List<IConnectionInfo> {connection1, connection2};

            var repo = Substitute.For<IConnectionInfoRepository>();
            repo.GetAll().Returns(connections);

            repo.When(z => z.RemoveBy(Arg.Any<Func<IConnectionInfo, bool>>())).Do(x => connections.RemoveAll(new ClosedSpecification().IsSatisfiedBy));

            var cm = new ConnectionMonitor(repo);
            cm.RemoveClosedConnection();

            var predicateReceived = (Func<IConnectionInfo, bool>) repo.ReceivedCalls()
                .First(x => x.GetMethodInfo().Name == "RemoveBy").GetArguments()[0];
            connectionsExpected.RemoveAll(new Predicate<IConnectionInfo>(predicateReceived));

            connections.Should().AllBeEquivalentTo(connectionsExpected);

            cm.GetConnectionsInfos().Where(new ClosedSpecification().IsSatisfiedBy).Should().BeEmpty();
        }

        [TestMethod]
        public void Sould_Return_Two_Connection_When_Adding_Two_Connection()
        {
            var connection1 = Substitute.For<IConnectionInfo>();
            var connection2 = Substitute.For<IConnectionInfo>();
            var repo = Substitute.For<IConnectionInfoRepository>();
            repo.GetAll().Returns(new List<IConnectionInfo> {connection1, connection2});
            var cm = new ConnectionMonitor(repo);
            cm.Add(connection1);
            cm.Add(connection2);
            repo.Received().Add(connection1);
            repo.Received().Add(connection2);

            cm.GetConnectionsInfos().Should().HaveCount(2);
        }

        #endregion
    }
}