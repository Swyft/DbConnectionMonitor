﻿namespace Utilities.Specifications
{
    public class AndNotSpecification<T> : CompositeSpecification<T>
    {
        #region Fields

        private readonly ISpecification<T> left;

        private readonly ISpecification<T> right;

        #endregion

        #region Constructors and Destructors

        public AndNotSpecification(ISpecification<T> left, ISpecification<T> right)
        {
            this.left = left;
            this.right = right;
        }

        #endregion

        #region Public Methods and Operators

        public override bool IsSatisfiedBy(T candidate)
        {
            return this.left.IsSatisfiedBy(candidate) && this.right.IsSatisfiedBy(candidate) != true;
        }

        #endregion
    }
}