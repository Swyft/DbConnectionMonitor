﻿namespace Utilities.Specifications
{
    public class NotSpecification<T> : CompositeSpecification<T>
    {
        #region Fields

        private readonly ISpecification<T> other;

        #endregion

        #region Constructors and Destructors

        public NotSpecification(ISpecification<T> other)
        {
            this.other = other;
        }

        #endregion

        #region Public Methods and Operators

        public override bool IsSatisfiedBy(T candidate)
        {
            return !this.other.IsSatisfiedBy(candidate);
        }

        #endregion
    }
}