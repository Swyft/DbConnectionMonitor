﻿namespace Utilities.Specifications
{
    public interface ISpecification<T>
    {
        #region Public Methods and Operators

        ISpecification<T> And(ISpecification<T> other);

        ISpecification<T> AndNot(ISpecification<T> other);

        bool IsSatisfiedBy(T candidate);

        ISpecification<T> Not();

        ISpecification<T> Or(ISpecification<T> other);

        ISpecification<T> OrNot(ISpecification<T> other);

        #endregion
    }
}