﻿using Utilities.Specifications;

namespace DbConnectionMonitor.Specifications
{
    public class ClosedSpecification : CompositeSpecification<IConnectionInfo>
    {
        #region Public Methods and Operators

        public override bool IsSatisfiedBy(IConnectionInfo candidate)
        {
            return new NotSpecification<IConnectionInfo>(new OpenedSpecification()).IsSatisfiedBy(candidate);
        }

        #endregion
    }
}