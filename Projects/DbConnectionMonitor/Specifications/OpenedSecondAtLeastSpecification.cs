namespace DbConnectionMonitor.Specifications
{
    using Utilities.Specifications;

    internal class OpenedSecondAtLeastSpecification : CompositeSpecification<IConnectionInfo>
    {
        #region Fields

        private readonly int secondeOpen;

        #endregion

        #region Constructors and Destructors

        public OpenedSecondAtLeastSpecification(int secondeOpen)
        {
            this.secondeOpen = secondeOpen;
        }

        #endregion

        #region Public Methods and Operators

        public override bool IsSatisfiedBy(IConnectionInfo candidate)
        {
            return candidate.TimeOpen.TotalSeconds >= this.secondeOpen;
        }

        #endregion
    }
}