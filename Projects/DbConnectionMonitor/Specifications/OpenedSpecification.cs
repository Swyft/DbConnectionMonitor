﻿namespace DbConnectionMonitor.Specifications
{
    using Utilities.Specifications;

    internal class OpenedSpecification : CompositeSpecification<IConnectionInfo>
    {
        #region Public Methods and Operators

        public override bool IsSatisfiedBy(IConnectionInfo candidate)
        {
            return candidate.IsOpen;
        }

        #endregion
    }
}