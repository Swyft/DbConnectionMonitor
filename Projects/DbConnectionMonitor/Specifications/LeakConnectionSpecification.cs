﻿namespace DbConnectionMonitor.Specifications
{
    using Utilities.Specifications;

    internal class LeakConnectionSpecification : CompositeSpecification<IConnectionInfo>
    {
        #region Public Methods and Operators

        public override bool IsSatisfiedBy(IConnectionInfo candidate)
        {
            return new AndSpecification<IConnectionInfo>(new OpenedSpecification(), new OpenedSecondAtLeastSpecification(180)).IsSatisfiedBy(candidate);
        }

        #endregion
    }
}