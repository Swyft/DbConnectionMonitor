﻿namespace DbConnectionMonitor
{
    using System;

    public interface IConnectionInfo
    {
        #region Public Properties

        bool IsOpen { get; set; }

        TimeSpan TimeOpen { get; }

        #endregion

        #region Public Methods and Operators

        void Close();

        #endregion
    }
}