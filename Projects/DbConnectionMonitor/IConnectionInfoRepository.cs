﻿namespace DbConnectionMonitor
{
    using System;
    using System.Collections.Generic;

    public interface IConnectionInfoRepository
    {
        #region Public Methods and Operators

        void Add(IConnectionInfo connectionInfo);

        IEnumerable<IConnectionInfo> GetAll();

        void RemoveAll();

        void RemoveBy(Func<IConnectionInfo, bool> predicate);

        #endregion
    }
}