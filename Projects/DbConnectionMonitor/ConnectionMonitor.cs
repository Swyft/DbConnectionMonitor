﻿namespace DbConnectionMonitor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DbConnectionMonitor.Specifications;

    using Utilities.Specifications;

    public class ConnectionMonitor
    {
        #region Fields

        private readonly IConnectionInfoRepository repository;

        #endregion

        #region Constructors and Destructors

        public ConnectionMonitor(IConnectionInfoRepository repository)
        {
            this.repository = repository;
        }

        #endregion

        #region Public Methods and Operators

        public void Add(IConnectionInfo connectionInfo)
        {
            this.repository.Add(connectionInfo);
        }

        public void Clear()
        {
            this.repository.RemoveAll();
        }

        public void CloseLeakConnectionsInfos()
        {
            foreach (var c in this.GetLeakConnectionsInfos())
            {
                c.Close();
            }
        }

        public IEnumerable<IConnectionInfo> GetConnectionsInfos()
        {
            return this.repository.GetAll();
        }

        public IEnumerable<IConnectionInfo> GetLeakConnectionsInfos()
        {
            return this.GetConnectionsInfos(new LeakConnectionSpecification());
        }

        public void RemoveClosedConnection()
        {
            this.repository.RemoveBy(new ClosedSpecification().IsSatisfiedBy);
        }

        #endregion

        #region Methods

        private ICollection<IConnectionInfo> GetConnectionsInfos(ISpecification<IConnectionInfo> specifications)
        {
            var leakConnectionInfos = new List<IConnectionInfo>();

            var connectionsInfos = this.repository.GetAll();

            if (connectionsInfos != null)
            {
                leakConnectionInfos = connectionsInfos.Where(specifications.IsSatisfiedBy).ToList();
            }

            return leakConnectionInfos;
        }

        #endregion
    }
}